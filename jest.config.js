module.exports = {
  coverageProvider: "v8",
  preset: 'ts-jest',
  testMatch: [
    "**/?(*.)+(spec|test).[tj]s?(x)"
  ]  
};
