import "reflect-metadata";
import express from 'express';

import { initializerRouter } from './router';

const app = express();

app.use(express.json());

initializerRouter(app);

export default app;
