import { Request, Response } from "express";
import { buyItems, findPurchase, listPurchases } from "../services/buy-service";

export const purchase = async (req: Request, res: Response) => {
    try {
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const items = await buyItems(req.user.id);
        if (!items) {
            return res.status(400).json({
                message: "Purchase failed"
            }); 
        }
        if (items[0] === undefined) {
            return res.status(400).json({
                message: "The cart is empty"
            }); 
        }
        return res.status(200).json({
            message: "Products purchased sucefully",
            purchased_items: items
        });
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }
    
}

export const getPurchase = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const purchase = await findPurchase(id, req.user.id, req.isAdm);
        if (!purchase) {
            return res.status(401).json({
                message: "Missing admin permission"
            });
        }
        return res.status(200).json(purchase);
    } catch {
        return res.status(500).json({
            message: "Internal error"
        });
    }
}

export const list = async (req: Request, res: Response) => {
    try {
        if (!req.isAdm) {
            return res.status(401).json({
                message: "Missing admin permission"
            });
        }
        const purchases = await listPurchases();
        return res.status(200).json(purchases);
    } catch (error) {
        return res.status(500).json({
            message: "Internal error"
        });
    }
}
