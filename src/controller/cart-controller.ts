import { Request, Response } from "express";
import { addProductToCart, deleteItemFromCart, findCart, listCarts } from "../services/cart-service";

export const add = async (req: Request, res: Response) => {
    try {
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const cartItem = await addProductToCart(req.body, req.user.id);
        if (cartItem === undefined) {
            return res.status(400).json({
                message: "Product not found or invalid token"
            });
        }
        return res.status(200).json({
            message: "Product added to cart",
            product: cartItem
        });   
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }
}

export const getCart = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const cart = await findCart(id, req.user.id, req.isAdm);
        if (!cart) {
            return res.status(401).json({
                message: "Missing admin permission"
            });
        }
        return res.status(200).json(cart);
    } catch {
        return res.status(500).json({
            message: "Internal error"
        });
    }
}

export const list = async (req: Request, res: Response) => {
    try {
        if (!req.isAdm) {
            return res.status(401).json({
                message: "Missing admin permission"
            });
        }
        const carts = await listCarts();
        return res.status(200).json(carts);
    } catch (error) {
        return res.status(500).json({
            message: "Internal error"
        });
    }
}

export const deleteItem = async (req: Request, res: Response) => {
    try {
        const { product_id } = req.params;
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const item = await deleteItemFromCart(product_id, req.user.id, req.isAdm);
        if (!item) {
            return res.status(401).json({
                message: "Missing admin permission"
            });
        }
        return res.status(200).json({
            message: "Product deleted from the cart",
            product: item
        });
    } catch {
        return res.status(500).json({
            message: "Internal error"
        });
    }
}
