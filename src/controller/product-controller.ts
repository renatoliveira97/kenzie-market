import { Request, Response } from "express";
import { checkProductExists, createProduct, findProduct, listProducts } from "../services/product-service";

export const create = async (req: Request, res: Response) => {
    try {
        if (!req.isAdm) {
            return res.status(401).json({
                message: "Unauthorized"
            });
        }
        const productExists = await checkProductExists(req.body);
        if (!productExists) {
            if (!req.user) {
                return res.status(500).json({
                    message: "Corrupted user"
                });
            }
            const product = await createProduct(req.body, req.user.id);
            return res.status(201).json(product);
        }
        return res.status(400).json({
            message: "Name already registered"
        });
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }
}

export const getProduct = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const product = await findProduct(id); 
        if (!product) {
            return res.status(400).json({
                message: "Product not exists"
            });
        }  
        return res.status(200).json(product);
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }  
}

export const list = async (req: Request, res: Response) => {
    const products = await listProducts();
    return res.status(200).json(products);
}
