import { Request, Response } from "express";
import { getCustomRepository } from "typeorm";
import UserRepository from "../repositories/user-repository";
import { checkUserExists, createUser, listUsers, UserBody } from "../services/user-service";

export const create = async (req: Request, res: Response) => {
    try {
        const userExists = await checkUserExists(req.body);
        if (!userExists) {
            const user = await createUser(req.body);
            return res.status(201).json(user);
        }
        return res.status(400).json({
            message: "Email already registered"
        })
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }
}

export const list = async (req: Request, res: Response) => {
    if (!req.isAdm) {
        return res.status(401).json({
            message: "Unauthorized"
        });
    }
    const users = await listUsers();
    return res.status(200).json(users);
}

export const getUser = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const userRepository = getCustomRepository(UserRepository);
        if (!req.user) {
            return res.status(500).json({
                message: "Corrupted user"
            });
        }
        const isOwner = id === req.user.id;
        if (!isOwner && !req.isAdm) {
            return res.status(401).json({
                message: "Unauthorized"
            });
        }
        const user: UserBody | undefined = await userRepository.findById(id);
        if (!user) {
            return res.status(500).json({
                message: "Corrupted id"
            });
        }
        delete user.password;    
        return res.status(200).json(user);
    } catch (error) {
        return res.status(500).json({
            message: "Internal Error"
        });
    }  
}

