import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import CartItem from './cartItem';
import Product from './product';

import User from './user';

@Entity('carts')
export default class Cart {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    name!: string;

    @ManyToOne(() => User, user => user.cart, { onDelete: 'CASCADE', cascade: true})
    owner!: User;

    @OneToMany(() => CartItem, cartItem => cartItem.cart)
    products!: CartItem[];
}
