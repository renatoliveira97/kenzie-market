import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import Cart from './cart';

@Entity('cart_items')
export default class CartItem {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    name!: string;

    @Column({ type: "float" })
    value!: number;

    @ManyToOne(() => Cart, cart => cart.products, { onDelete: 'CASCADE', cascade: true })
    cart!: Cart;

}
