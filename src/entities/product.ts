import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import Cart from './cart';

@Entity('products')
export default class Product {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    name!: string;

    @Column({ type: "float" })
    value!: number;

}
