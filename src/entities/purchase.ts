import { Entity, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, OneToMany } from 'typeorm';
import PurchaseItem from './purchaseItem';

import User from './user';

@Entity('purchases')
export default class Purchase {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @CreateDateColumn()
    purchasedAt!: Date;

    @ManyToOne(() => User, user => user.cart, { onDelete: 'CASCADE', cascade: true})
    user!: User;

    @OneToMany(() => PurchaseItem, purchaseItem => purchaseItem.purchase)
    products!: PurchaseItem[];
}