import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import Purchase from './purchase';

@Entity('purchases_items')
export default class PurchaseItem {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    name!: string;

    @Column({ type: "float" })
    value!: number;

    @ManyToOne(() => Purchase, purchase => purchase.products, { onDelete: 'CASCADE', cascade: true })
    purchase!: Purchase;

}
