import bcrypt from 'bcrypt';
import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, OneToMany } from 'typeorm';
import Cart from './cart';
import Purchase from './purchase';

@Entity('users')
export default class User {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    name!: string;

    @Column({ unique: true })
    email!: string;

    @Column()
    password!: string;
    
    @Column()
    isAdm!: boolean;  

    @OneToMany(() => Cart, cart => cart.owner)
    cart!: Cart[];

    @OneToMany(() => Purchase, purchase => purchase.user)
    purchases!: Purchase[];

    @BeforeInsert()
    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 10);
    }
}
