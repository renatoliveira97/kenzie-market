import { Request, Response, NextFunction } from 'express';
import { getCustomRepository } from "typeorm";

import UserRepository from "../repositories/user-repository";

export const checkIsAdm = async (req: Request, res: Response, next: NextFunction) => {
    const userRepository = getCustomRepository(UserRepository);
    if (req.user === undefined) {
        return res.status(500).json({
            message: "Corrupted user"
        });
    }
    const user = await userRepository.findById(req.user.id);
    if (!user) {
        return res.status(500).json({
            message: "Corrupted id"
        });
    }
    req.isAdm = user.isAdm;
    next();
}
