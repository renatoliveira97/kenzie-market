import { Request, Response, NextFunction } from "express";

export const validateCartData = async (req: Request, res: Response, next: NextFunction) => {
    const { productId } = req.body;
    try {
        if (!productId) {
            return res.status(400).json({
                message: 'Missing parameter productId'
            });
        }
        next();
    } catch (error) {
        return res.status(500).json({
            message: "Internal error"
        })
    }
}
