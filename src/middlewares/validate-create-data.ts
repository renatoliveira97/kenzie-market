import { Request, Response, NextFunction } from "express";

export const validateCreateData = async (req: Request, res: Response, next: NextFunction) => {
    const { name, email, password, isAdm } = req.body;
    try {
        if (!name) {
            return res.status(400).json({
                message: 'Missing parameter name'
            });
        }
        if (!email) {
            return res.status(400).json({
                message: 'Missing parameter email'
            });
        }
        if (!password) {
            return res.status(400).json({
                message: 'Missing parameter password'
            });
        }
        if (isAdm === undefined) {
            return res.status(400).json({
                message: 'Missing parameter isAdm'
            });
        }
        next();
    } catch (error) {
        return res.status(500).json({
            message: "Internal error"
        })
    }
}
