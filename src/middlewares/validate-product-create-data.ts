import { Request, Response, NextFunction } from "express";

export const validateProductCreateData = async (req: Request, res: Response, next: NextFunction) => {
    const { name, value } = req.body;
    try {
        if (!name) {
            return res.status(400).json({
                message: 'Missing parameter name'
            });
        }
        if (!value) {
            return res.status(400).json({
                message: 'Missing parameter value'
            });
        }
        if (typeof value !== "number") {
            return res.status(400).json({
                message: 'Value needs to be a number'
            });
        }
        next();
    } catch (error) {
        return res.status(500).json({
            message: "Internal error"
        })
    }
}
