import { EntityRepository, Repository } from "typeorm";
import Cart from "../entities/cart";
import User from "../entities/user";

@EntityRepository(Cart)
class CartRepository extends Repository<Cart> {
    public async findUserCart(name: string, user: User): Promise<Cart | undefined> {
        const cart = await this.findOne({
            where: {
                name,
                owner: user
            }
        });
        return cart;
    }
}

export default CartRepository;
