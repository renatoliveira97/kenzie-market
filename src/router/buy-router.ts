import { Router } from "express";
import { getPurchase, list, purchase } from "../controller/buy-controller";
import { authenticate } from "../middlewares/authentication";
import { checkIsAdm } from "../middlewares/check-is-adm";

const router = Router();

export const buyRouter = () => {
    router.post('', authenticate, purchase);
    router.get('/:id', authenticate, checkIsAdm, getPurchase);
    router.get('', authenticate, checkIsAdm, list);
    return router;
}