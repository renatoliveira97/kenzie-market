import { Router } from "express";
import { add, deleteItem, getCart, list } from "../controller/cart-controller";
import { authenticate } from "../middlewares/authentication";
import { checkIsAdm } from "../middlewares/check-is-adm";
import { validateCartData } from "../middlewares/validate-cart-data";

const router = Router();

export const cartRouter = () => {
    router.post('', validateCartData, authenticate, add);
    router.get('/:id', authenticate, checkIsAdm, getCart);
    router.get('', authenticate, checkIsAdm, list);
    router.delete('/:product_id', authenticate, checkIsAdm, deleteItem);
    return router;
}
