import { Express } from "express";
import { buyRouter } from "./buy-router";
import { cartRouter } from "./cart-router";
import { loginRouter } from "./login-router";
import { productRouter } from "./product-router";
import { userRouter } from "./user-router";

export const initializerRouter = (app: Express) => {
    app.use('/user', userRouter());
    app.use('/login', loginRouter());
    app.use('/product', productRouter());
    app.use('/cart', cartRouter());
    app.use('/buy', buyRouter());
}
