import { Router } from "express";
import { login } from "../controller/login-controller";
import { validateLoginData } from "../middlewares/validate-login-data";

const router = Router();

export const loginRouter = () => {
    router.post('', validateLoginData, login);
    return router;
}
