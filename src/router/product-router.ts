import { Router } from "express";
import { create, getProduct, list } from "../controller/product-controller";
import { authenticate } from "../middlewares/authentication";
import { checkIsAdm } from "../middlewares/check-is-adm";
import { validateProductCreateData } from "../middlewares/validate-product-create-data";

const router = Router();

export const productRouter = () => {
    router.post('', validateProductCreateData, authenticate, checkIsAdm, create);
    router.get('/:id', getProduct);
    router.get('', list);
    return router;
}
