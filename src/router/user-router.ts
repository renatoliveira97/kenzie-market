import { Router } from "express";
import { create, getUser, list } from "../controller/user-controller";
import { authenticate } from "../middlewares/authentication";
import { checkIsAdm } from "../middlewares/check-is-adm";
import { validateCreateData } from "../middlewares/validate-create-data";

const router = Router();

export const userRouter = () => {
    router.post('', validateCreateData, create);
    router.get('/:id', authenticate, checkIsAdm, getUser);
    router.get('', authenticate, checkIsAdm, list);
    return router;
}
