import { getRepository } from "typeorm";
import Cart from "../entities/cart";
import User from "../entities/user";
import PurchaseItem from "../entities/purchaseItem";
import CartItem from "../entities/cartItem";
import Purchase from "../entities/purchase";
import { mailTemplateOptions, transport } from "./email-service";

export const buyItems = async (userId: string) => {
    try {
        const userRepository = getRepository(User);
        const cartRepository = getRepository(Cart);
        const cartItemRepository = getRepository(CartItem);
        const purchaseItemRepository = getRepository(PurchaseItem);
        const purchaseRepository = getRepository(Purchase);
        const user = await userRepository.findOne(userId);
        if (!user) {
            throw new Error();
        }
        const cart = await cartRepository.findOne({
            where: {
                name: "default",
                owner: user
            },
            relations: ['products']
        });
        if (!cart) {
            throw new Error();
        }
        const purchase = purchaseRepository.create({
            user,
            purchasedAt: new Date()
        });
        await purchaseRepository.save(purchase);
        const products: CartItem[] = cart.products;
        const items = [];
        let total = 0.0;
        for (let product of products) {
            let item = purchaseItemRepository.create({
                name: product.name,
                value: product.value,
                purchase
            }); 
            await purchaseItemRepository.save(item);
            items.push(item);
            total += item.value;
            await cartItemRepository.remove([product]);
        }
        if (items[0] === undefined) {
            return items;
        }
        const options = mailTemplateOptions(
            [user.email],
            'Confirmação de Compra',
            'purchase',
            {
                name: user.name,
                value: total,
                date: purchase.purchasedAt
            }
        );

        transport.sendMail(options, function (err, info) {
            if (err) {
                return undefined;
            } else {
                console.log(info);
            }
        });
        return items;
    } catch (error) {
        return undefined;
    }    
}

export const findPurchase = async (id: string, userId: string, isAdm: boolean | undefined) => {
    const userRepository = getRepository(User);
    const purchaseRepository = getRepository(Purchase);
    const user = await userRepository.findOne(userId);
    if (!user) {
        throw new Error();
    }
    const purchase = await purchaseRepository.findOne({
        where: {
            id
        },
        relations: ['user']
    });
    if (!purchase) {
        throw new Error();
    }
    const purchaseUser: User = purchase.user;
    const isOwner = userId === purchaseUser.id;
    if (!isOwner && !isAdm) {
        return undefined;
    }
    return purchase;
}

export const listPurchases = async () => {
    const purchaseRepository = getRepository(Purchase);
    const purchases = await purchaseRepository.find({
        relations: ['products', 'user']
    });
    return purchases;
}
