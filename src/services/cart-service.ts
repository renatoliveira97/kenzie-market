import { getCustomRepository, getRepository } from "typeorm";
import Cart from "../entities/cart";
import CartItem from "../entities/cartItem";
import Product from "../entities/product";
import User from "../entities/user";
import CartRepository from "../repositories/cart-repository";
import { UserBody } from "./user-service";

export interface AddProductBody {
    productId: string
}


export const addProductToCart = async (body: AddProductBody, userId: string) => {
    try {
        const { productId } = body;
        const productRepository = getRepository(Product);
        const cartRepository = getCustomRepository(CartRepository);
        const userRepository = getRepository(User);
        const cartItemRepository = getRepository(CartItem);
        const product = await productRepository.findOne(productId);
        if (!product) {
            throw new Error();
        }
        const user = await userRepository.findOne(userId);
        if (!user) {
            throw new Error();
        }
        let cart = await cartRepository.findUserCart("default", user);
        if (cart === undefined) {
            cart = cartRepository.create({
                name: "default",
                owner: user
            });
            await cartRepository.save(cart);
        }
        const cartItem = cartItemRepository.create({
            name: product.name,
            value: product.value,
            cart: cart
        });
        await cartItemRepository.save(cartItem);
        return cartItem;
    } catch (error) {
        return undefined;
    }  
}

export const findCart = async (id: string, userId: string, isAdm: boolean | undefined) => {
    const userRepository = getRepository(User);
    const cartRepository = getRepository(Cart);
    const user = await userRepository.findOne(userId);
    if (!user) {
        throw new Error();
    }
    const cart = await cartRepository.findOne({
        where: {
            id
        },
        relations: ['owner']
    });
    if (!cart) {
        throw new Error();
    }
    const cartUser: User = cart.owner;
    const isOwner = userId === cartUser.id;
    if (!isOwner && !isAdm) {
        return undefined;
    }
    return cart;
}

export const listCarts = async () => {
    const cartRepository = getRepository(Cart);
    const carts = await cartRepository.find({
        relations: ['products', 'owner']
    });
    return carts;
}

export const deleteItemFromCart = async (id: string, userId: string, isAdm: boolean | undefined) => {
    const userRepository = getRepository(User);
    const cartRepository = getRepository(Cart);
    const cartItemRepository = getRepository(CartItem);
    const user = await userRepository.findOne(userId);
    if (!user) {
        throw new Error();
    }
    const item = await cartItemRepository.findOne({
        where: {
            id
        },
        relations: ['cart']
    });
    if (!item) {
        throw new Error();
    }
    const cart = await cartRepository.findOne({
        where: {
            id: item.cart.id
        },
        relations: ['owner']
    });
    if (!cart) {
        throw new Error();
    }
    const cartUser: User = cart.owner;
    const isOwner = userId === cartUser.id;
    if (!isOwner && !isAdm) {
        return undefined;
    }
    await cartItemRepository.remove([item]);
    return item;
}
