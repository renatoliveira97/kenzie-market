import nodemailer from 'nodemailer';
import path from 'path';
import hbs, {NodemailerExpressHandlebarsOptions} from 'nodemailer-express-handlebars';


export const transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "6e2215a8e950ff",
      pass: "e22981d0be8328"
    }
});

const handlebarOption: NodemailerExpressHandlebarsOptions = {
    viewEngine: {
        partialsDir: path.resolve(__dirname, '..', 'templates'),
        defaultLayout: undefined
    },
    viewPath: path.resolve(__dirname, '..', 'templates')
}

transport.use('compile', hbs(handlebarOption));


export const mailOptions = (to: string[], subject: string, text: string) => {
    return {
        from: 'no-repply@kenmarket.com.br',
        to,
        subject,
        text
    };
};

export const mailTemplateOptions = (to: string[], subject: string, template: string, context: any) => {
    return {
        from: 'no-repply@kenmarket.com.br',
        to,
        subject,
        template,
        context
    };
}
