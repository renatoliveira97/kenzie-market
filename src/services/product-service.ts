import { getCustomRepository, getRepository } from "typeorm";
import User from "../entities/user";
import Cart from "../entities/cart";
import Product from "../entities/product";
import ProductRepository from "../repositories/product-repository";

export interface ProductBody {
    name: string,
    value: number
}

export const checkProductExists = async (body: ProductBody) => {
    const { name } = body;
    try {
        const productRepository = getCustomRepository(ProductRepository);
        const product = await productRepository.findByName(name);
        if (product === undefined) {
            return false;
        }
        return true;
    } catch (error) {
        throw new Error();
    }
}

export const createProduct = async (body: ProductBody, userId: string) => {
    const { name, value } = body;
    const productRepository = getRepository(Product);
    const product = productRepository.create({
        name,
        value
    });
    await productRepository.save(product);
    return product;
}

export const findProduct = async (id: string) => {
    try {
        const productRepository = getRepository(Product);
        const product = await productRepository.findOne(id);
        return product;
    } catch (error) {
        return undefined;
    }
}

export const listProducts = async () => {
    const productRepository = getRepository(Product);
    const products = await productRepository.find(undefined);
    return products;
}
