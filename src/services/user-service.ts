import { getRepository, getCustomRepository } from "typeorm";
import User from "../entities/user";

import UserRepository from "../repositories/user-repository";

export interface UserBody {
    name: string,
    email: string,
    password?: string,
    isAdm: boolean
}

export const checkUserExists = async (body: UserBody) => {
    const { email } = body;
    try {
        const userRepository = getCustomRepository(UserRepository);
        const user = await userRepository.findByEmail(email);
        if (user === undefined) {
            return false;
        }
        return true;
    } catch (error) {
        throw new Error();
    }
}

export const createUser = async (body: UserBody) => {
    const { name, email, password, isAdm } = body;
    try {
        const userRepository = getRepository(User);
        const user = userRepository.create({
            name,
            email,
            password,
            isAdm,
        });
        await userRepository.save(user);
        let userCopy: UserBody = { ...user };
        delete userCopy.password;
        return userCopy;
    } catch (error) {
        throw new Error();
    }
}

export const listUsers = async () => {
    const userRepository = getRepository(User);
    const users: UserBody[] = await userRepository.find(undefined);
    for (let user of users) {
        delete user.password;
    }
    return users;
}
